<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::post('/get-dishes-by-prods', 'FrontController@getDishesByProds');

Auth::routes();

Route::get('/panel/dishes/get-dishes', 'Panel\DishesController@getDishes');
Route::resource('/panel/dishes', 'Panel\DishesController');

Route::get('/panel/get-prods', 'Panel\ProductsController@getProds');
Route::get('/panel/get-prods-name', 'Panel\ProductsController@getProdsName');
Route::resource('/panel', 'Panel\ProductsController');