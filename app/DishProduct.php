<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishProduct extends Model
{
    protected $table = 'dishes_products';
    protected $fillable = ['product_id', 'dish_id'];
}
