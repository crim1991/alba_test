<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        return view('front.index');
    }

    public function getDishesByProds(Request $request)
    {
        $data = $request->all();

        $dishes = Dish::select('dishes.id', 'dishes.name')
            ->leftJoin('dishes_products', 'dish_id', 'dishes.id')
            ->whereIn('product_id', $data['prod_ids'])
            ->distinct()
            ->get()->toArray();

        return $dishes;
    }
}
