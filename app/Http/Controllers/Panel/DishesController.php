<?php

namespace App\Http\Controllers\Panel;

use App\Dish;
use App\DishProduct;
use App\Http\Controllers\HomeController;
use App\Http\Requests\StoreDishRequest;
use Illuminate\Http\Request;

class DishesController extends HomeController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'dishes';
        return view('panel.dishes.index', compact('active'));
    }

    public function getDishes()
    {
        return Dish::select('id', 'name')->get()->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDishRequest $request)
    {
        $data = $request->except('prods_ids');
        $dish_id = Dish::create($data)->id;

        $prods_ids = $request->only('prods_ids');

        foreach ($prods_ids['prods_ids'] as $prod) {
            DishProduct::create([
                'product_id' => $prod['id'],
                'dish_id' => $dish_id
            ]);
        }

        return $dish_id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dish::destroy($id);
    }
}
